import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// Create 2 ArrayLists one for Dwarves and one for Dwarfs
		
		ArrayList<String> dwarfs = new ArrayList<String>();
		dwarfs.add("Happy");
		dwarfs.add("Sleepy");
		dwarfs.add("Dopey");
		dwarfs.add("Bashful");
		dwarfs.add("Sneezy");
		dwarfs.add("Doc");
		dwarfs.add("Grumpy");
		
		
		ArrayList<String> dwarves = new ArrayList<String>();
		dwarves.add("Thorin");
		dwarves.add("Balin");
		dwarves.add("Dwalin");
		dwarves.add("Fili");
		dwarves.add("Kili");
		dwarves.add("Dori");
		dwarves.add("Nori");
		dwarves.add("Oin");
		dwarves.add("Ori");
		dwarves.add("Gloin");
		dwarves.add("Bifur");
		dwarves.add("Bofur");
		dwarves.add("Bombur");

		System.out.println("The Disney Dwarfs are:");
		
		for (String dwarf : dwarfs) {
			System.out.println(dwarf);

		}
		System.out.println("The Tolkien Dwarves are:");

		for (String dwarf : dwarves) {
			System.out.println(dwarf);

		}
	}

}
