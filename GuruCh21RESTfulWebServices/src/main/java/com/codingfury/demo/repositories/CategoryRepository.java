package com.codingfury.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.codingfury.demo.domain.Category;


public interface CategoryRepository extends JpaRepository<Category, Long> {

}
