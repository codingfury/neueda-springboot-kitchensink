package com.codingfury.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuruCh21ResTfulWebServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuruCh21ResTfulWebServicesApplication.class, args);
	}
}
