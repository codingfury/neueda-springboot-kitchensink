package uk.ac.belfastmet.niassembly.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import uk.ac.belfastmet.niassembly.domain.AllMembersAPI;
import uk.ac.belfastmet.niassembly.domain.AllMembersList;

@Controller
@RequestMapping
public class MLAController {

	@GetMapping()
	public String home() {
		
		String allMembersForConstituency1Url = "http://data.niassembly.gov.uk/members_json.ashx?m=GetAllCurrentMembersByConstituencyId&constituencyId=1";
		RestTemplate restTemplate = new RestTemplate();
		
		AllMembersAPI allMembersAPI = restTemplate.getForObject(allMembersForConstituency1Url, AllMembersAPI.class);
		AllMembersList allMembersList = allMembersAPI.getAllMembersList();
		
		Logger logger = LoggerFactory.getLogger(MLAController.class);
		logger.info(allMembersList.toString());
		
		return "index";
		
	}
	
	
}
