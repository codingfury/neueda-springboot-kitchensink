package com.codingfury.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuruCh21ResTfulWebServices1Application {

	public static void main(String[] args) {
		SpringApplication.run(GuruCh21ResTfulWebServices1Application.class, args);
	}
}
