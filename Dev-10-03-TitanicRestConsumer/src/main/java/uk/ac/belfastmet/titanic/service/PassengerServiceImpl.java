package uk.ac.belfastmet.titanic.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import uk.ac.belfastmet.titanic.domain.Passenger;

public class PassengerServiceImpl implements PassengerService {

	
	@Value("${api.titanicUrl}") 
	private String titanicUrl;
	
	
	
	
	public PassengerServiceImpl(String titanicUrl, RestTemplate restTemplate) {
		super();
		this.titanicUrl = titanicUrl;
	}

	@Override
	public ArrayList<Passenger> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Passenger get(Integer passengerId) {
		// TODO Auto-generated method stub
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(titanicUrl + "/" + Integer.toString(passengerId));
		
		Passenger passenger = this.restTemplate.getForObject(uriBuilder.toString(), Passenger.class);

		return passenger;
	}

	@Override
	public Passenger post(Passenger passenger, Integer passengerId) {
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(titanicUrl + "/" + Integer.toString(passengerId));
		
		Map<String,Object> passengerMap = new HashMap<String, Object>();
		passengerMap.put("survived", passenger.getSurvived());
		passengerMap.put("pclass", passenger.getPclass());
		passengerMap.put("name", passenger.getName());
		passengerMap.put("sex", passenger.getSex());
		passengerMap.put("age", passenger.getAge());
		passengerMap.put("sibSp", passenger.getSibSp());
		passengerMap.put("parch", passenger.getParch());
		passengerMap.put("ticket", passenger.getTicket());
		passengerMap.put("fare", passenger.getFare());
		passengerMap.put("cabin", passenger.getCabin());
		passengerMap.put("embarked", passenger.getEmbarked());
		
		
		
		Passenger savedPassenger = this.restTemplate.postForObject(uriBuilder.toString(), passengerMap, Passenger.class);
		return savedPassenger;
	}

	@Override
	public void delete(Integer passengerId) {
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(titanicUrl + "/" + Integer.toString(passengerId));
		
		this.restTemplate.delete(uriBuilder.toString());

		

	}

}
