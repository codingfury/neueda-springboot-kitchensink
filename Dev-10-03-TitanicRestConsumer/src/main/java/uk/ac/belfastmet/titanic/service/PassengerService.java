package uk.ac.belfastmet.titanic.service;

import java.util.ArrayList;

import uk.ac.belfastmet.titanic.domain.Passenger;

public interface PassengerService {

	public ArrayList<Passenger> list();
	public Passenger get(Integer passengerId);
	public Passenger post(Passenger passenger, Integer passengerId);
	public void delete(Integer passengerId);
	
}
