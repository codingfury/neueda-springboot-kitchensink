package uk.ac.belfastmet.dwarfs.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import uk.ac.belfastmet.dwarfs.domain.Dwarf;
import uk.ac.belfastmet.dwarfs.repository.DwarfRepository;
import uk.ac.belfastmet.dwarfs.service.DwarfService;

@Controller
@RequestMapping("/")
public class DwarfController {

	DwarfRepository dwarfRepository;
	
	public DwarfController(DwarfRepository dwarfRepository) {
		super();
		this.dwarfRepository = dwarfRepository;
	}

	@GetMapping("/dwarfs")
	public String allDwarfs(Model model) {
		model.addAttribute("pageTitle","All");
		model.addAttribute("dwarfs", this.dwarfRepository.findAll());
		return "dwarfPage";
	}
	
	@GetMapping("/view/{dwarfId}")
	public String viewDwarf(@PathVariable("dwarfId") Integer dwarfId, Model model) {
		
		model.addAttribute("pageTitle", "View Dwarf");
		model.addAttribute("dwarf", this.dwarfRepository.findByDwarfId(dwarfId));
		return "viewDwarf";
	}
	
	@GetMapping("/add")
	public String addDwarf(Model model) {
		
		model.addAttribute("pageTitle", "Add");
		model.addAttribute("dwarf", new Dwarf());
		return "editDwarf";
	}
	
	@GetMapping("/edit/{dwarfId}")
	public String editDwarf(@PathVariable("dwarfId") Integer dwarfId, Model model) {
		
		model.addAttribute("pageTitle", "Edit");
		model.addAttribute("dwarf", this.dwarfRepository.findByDwarfId(dwarfId));
		return "editDwarf";
	}
	
	@PostMapping("/save")
	public String saveDwarf(@Valid Dwarf dwarf, BindingResult bindingResult, Model model) {
		
		if(bindingResult.hasErrors()) {
			model.addAttribute("pageTitle", "Add");
			return "editDwarf";
		}else {
			Dwarf savedDwarf = this.dwarfRepository.save(dwarf);
			return "redirect:/view/" + savedDwarf.getDwarfId();
		}
	}
	
	
	@GetMapping("/delete/{dwarfId}")
	public String deleteDwarf(@PathVariable("dwarfId") Integer dwarfId, 
			RedirectAttributes redirectAttribute ,Model model) {
		
		redirectAttribute.addFlashAttribute("message", "Successfully deleted dwarf!");
		
		this.dwarfRepository.delete(dwarfId);
		return "redirect:/dwarfs";
	}
	
	
	
}
