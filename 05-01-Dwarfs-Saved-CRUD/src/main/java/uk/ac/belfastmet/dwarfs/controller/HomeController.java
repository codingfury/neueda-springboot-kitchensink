package uk.ac.belfastmet.dwarfs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import uk.ac.belfastmet.dwarfs.repository.DwarfRepository;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	DwarfRepository dwarfRepository;
	
	public HomeController(DwarfRepository dwarfRepository) {
		super();
		this.dwarfRepository = dwarfRepository;
	}

	@GetMapping("/")
	public String disney(Model model) {
		model.addAttribute("pageTitle","Dwarfs!");
		return "homePage";
	}
	
	@GetMapping("/searchid")
	public String searchById(@RequestParam("id") Integer dwarfId, Model model) {
		
		model.addAttribute("pageTitle","Dwarfs!");
		model.addAttribute("dwarfs",this.dwarfRepository.findByDwarfId(dwarfId));
		return "dwarfPage";
	}
	
	@PostMapping("/searchname")
	public String searchByName(@RequestParam("name") String name, Model model) {
		
		model.addAttribute("pageTitle","Dwarfs!");
		model.addAttribute("dwarfs",this.dwarfRepository.findByName(name));
		return "dwarfPage";
	}
	
}
