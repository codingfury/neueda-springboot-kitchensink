package uk.ac.belfastmet.titanic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uk.ac.belfastmet.titanic.domain.Passenger;
import uk.ac.belfastmet.titanic.service.PassengerService;

@RestController
@RequestMapping("/api")
public class PassengerRestController {

	@Autowired
	private PassengerService passengerService;

	public PassengerRestController(PassengerService passengerService) {
		super();
		this.passengerService = passengerService;
	}
	
	
	
	@GetMapping("/")
	public Iterable<Passenger> list(){
		return passengerService.list();
	}
	
	@PostMapping("/")
	public Passenger create(@RequestBody Passenger passenger){
		return passengerService.create(passenger);
	}
	
	@GetMapping("/{passengerId}")
	public Passenger read(@PathVariable("passengerId") Integer passengerId) {
		
		return passengerService.read(passengerId);
		
	}
	
	@PutMapping("/{passengerId}")
	public Passenger update(@PathVariable("passengerId") Integer passengerId, @RequestBody Passenger passenger) {
		
		return passengerService.update(passengerId, passenger);
	}
	
	@DeleteMapping("/{passengerId}")
	public void delete(@PathVariable("passengerId") Integer passengerId) {
		
		passengerService.delete(passengerId);
	}
}
