package uk.ac.belfastmet.titanic.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uk.ac.belfastmet.titanic.domain.Passenger;
import uk.ac.belfastmet.titanic.repositories.PassengerRepository;

@Service
public class PassengerServiceImpl implements PassengerService {

	@Autowired
	private PassengerRepository passengerRepository;
	
	
	
	public PassengerServiceImpl(PassengerRepository passengerRepository) {
		super();
		this.passengerRepository = passengerRepository;
	}

	@Override
	public Iterable<Passenger> list() {
		return this.passengerRepository.findAll();
	}

	@Override
	public Passenger create(Passenger passenger) {
		
		return this.passengerRepository.save(passenger);
	}

	@Override
	public Passenger read(Integer passengerId) {

		return this.passengerRepository.findOne(passengerId);
	}

	@Override
	@Transactional //not needed unless 2 tables involved
	public Passenger update(Integer passengerId, Passenger update) {
		
		Passenger passenger = this.passengerRepository.findOne(passengerId);
		
		return passengerRepository.save(passenger);
	}

	@Override
	public void delete(Integer passengerId) {
		// TODO Auto-generated method stub
		this.passengerRepository.delete(passengerId);

	}

}
