package uk.ac.belfastmet.weather.domain;

import lombok.Data;

@Data
public class WeatherAPI {
	
	Location location;
	Weather weather;
}
