package uk.ac.belfastmet.weather.domain;

import lombok.Data;

@Data
public class Wind {

	Integer speed;
	String direction;
}
