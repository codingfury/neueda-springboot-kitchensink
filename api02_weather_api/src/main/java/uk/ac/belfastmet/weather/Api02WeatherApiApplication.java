package uk.ac.belfastmet.weather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Api02WeatherApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Api02WeatherApiApplication.class, args);
	}

}
