package uk.ac.belfastmet.weather.domain;

import lombok.Data;

@Data
public class Location {

	String name;
	Float latitude;
	Float longitude;
	
}
