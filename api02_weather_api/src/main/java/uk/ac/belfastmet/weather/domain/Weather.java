package uk.ac.belfastmet.weather.domain;

import lombok.Data;

@Data
public class Weather {

	String description;
    String precipitation;
    Integer temperature;
    String humidity;
    Integer pressure;
    Integer cloudcover;
    Wind wind;
    String image;
	
}
