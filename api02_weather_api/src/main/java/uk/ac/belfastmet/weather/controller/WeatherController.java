package uk.ac.belfastmet.weather.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;

import uk.ac.belfastmet.weather.domain.WeatherAPI;

@Controller
public class WeatherController {

	@GetMapping("")
	public String home(Model model) {
		
		return "index";
	}

	@GetMapping("/weather/{city}")
	public String weather(@PathVariable("city")String city,  Model model) {
		
		String belfastWeatherUrl =  "http://localhost:8888/weathersample/weather.php?location=belfast";
		
		RestTemplate restTemplate = new RestTemplate(); 
		WeatherAPI weatherAPI = restTemplate.getForObject(belfastWeatherUrl, WeatherAPI.class);
		
		// Logger logger = LoggerFactory.getLogger(WeatherController.class);
		// logger.info(weatherMap.toString());
		
		model.addAttribute("city", city);
		model.addAttribute("weatherAPI", weatherAPI);
		return "weather";
	}

}


