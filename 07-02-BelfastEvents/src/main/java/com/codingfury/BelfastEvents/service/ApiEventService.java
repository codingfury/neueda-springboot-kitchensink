package com.codingfury.BelfastEvents.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.codingfury.BelfastEvents.model.AllEvents;
import com.codingfury.BelfastEvents.model.Event;



@Service
public class ApiEventService {
	
	private RestTemplate restTemplate;
	
	@Value("${api.events.url}")  //from the properties file
	private String apiEventUrl;
	
	public ApiEventService(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}
	
	
	public ArrayList<Event> getEvents() {
		this.apiEventUrl = "http://neueda-flask-bndouglas.c9users.io/belfast-events/api/"; //BND
	    UriComponentsBuilder uriBuilder = UriComponentsBuilder
	            .fromUriString(this.apiEventUrl);
	            //.queryParam("limit", limit);

	    AllEvents allEvents = restTemplate.getForObject(uriBuilder.toUriString(), AllEvents.class);
	    return allEvents.getAllEvents();
	}

}




