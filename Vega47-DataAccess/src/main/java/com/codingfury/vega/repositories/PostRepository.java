package com.codingfury.vega.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.vega.domain.Post;

public interface PostRepository extends CrudRepository<Post, Long> {

	
	
}
