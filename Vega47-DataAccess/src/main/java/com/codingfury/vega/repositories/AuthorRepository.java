package com.codingfury.vega.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.vega.domain.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
