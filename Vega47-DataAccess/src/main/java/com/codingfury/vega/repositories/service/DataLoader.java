package com.codingfury.vega.repositories.service;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codingfury.vega.domain.Author;
import com.codingfury.vega.domain.Post;
import com.codingfury.vega.repositories.AuthorRepository;
import com.codingfury.vega.repositories.PostRepository;

@Service
public class DataLoader {
	
	private PostRepository postRepository;
	private AuthorRepository authorRepository;
	
	@Autowired
	public DataLoader(PostRepository postRepository, AuthorRepository authorRepository) {
		this.postRepository = postRepository;
		this.authorRepository = authorRepository;
	}
	
	@PostConstruct
	private void loadData() {
		Author dv = new Author("Dan", "Vega");
		authorRepository.save(dv);
		
		Post post = new Post("Spring Data Rocks");
		post.setBody("Post body here");
		post.setPostedOn(new Date());
		post.setAuthor(dv);
		postRepository.save(post);
		
	}
	
}
