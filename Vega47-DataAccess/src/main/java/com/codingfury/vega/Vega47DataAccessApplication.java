package com.codingfury.vega;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.codingfury.vega.domain.Post;
import com.codingfury.vega.repositories.PostRepository;
import com.codingfury.vega.repositories.service.DataLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringBootApplication
public class Vega47DataAccessApplication {

	private static final Logger logger = LoggerFactory.getLogger(Vega47DataAccessApplication.class);
	
	@Autowired
	PostRepository postRepository;
	
	@Autowired
	DataLoader dataLoader;
	
	public static void main(String[] args) {
		SpringApplication.run(Vega47DataAccessApplication.class, args);
		
	}
	
	
	@PostConstruct  //run after everything has been set up
	void seePosts() {
		logger.info("seePosts method called..");
		for(Post post: postRepository.findAll()) {
			logger.info(post.toString());
		}
		
	}
}
