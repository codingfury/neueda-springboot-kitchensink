package uk.ac.belfastmet.dwarfs.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Dwarf {
	
	
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)	//Default
	@GeneratedValue(strategy = GenerationType.IDENTITY)  //make sure the mysql table is AI and PK
	private Integer dwarfId;
	@NotEmpty
	private String name;
	private String author;
	private String image;
	
	
	public Dwarf() {
		super();
	}
	
	
	public Dwarf(String name, String author, String image) {
		super();
		this.name = name;
		this.author = author;
		this.image = image;
	}

	

	public Integer getDwarfId() {
		return dwarfId;
	}


	public void setDwarfId(Integer dwarfId) {
		this.dwarfId = dwarfId;
	}


	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}
