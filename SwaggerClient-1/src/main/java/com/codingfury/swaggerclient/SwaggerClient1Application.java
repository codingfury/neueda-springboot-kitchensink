package com.codingfury.swaggerclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerClient1Application {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerClient1Application.class, args);
	}
}
