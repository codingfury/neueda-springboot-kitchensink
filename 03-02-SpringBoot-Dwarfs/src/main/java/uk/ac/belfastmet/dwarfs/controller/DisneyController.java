package uk.ac.belfastmet.dwarfs.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uk.ac.belfastmet.dwarfs.domain.Dwarf;
import uk.ac.belfastmet.dwarfs.service.DwarfService;

@Controller
@RequestMapping("/disney")
public class DisneyController {

	@GetMapping("/")
	public String disney(Model model) {
		model.addAttribute("pageTitle","Disney!");
		DwarfService dwarfService = new DwarfService();
		model.addAttribute("dwarfs", dwarfService.getDisneyDwarfs());
		return "disneyPage";
	}
	

	
	
	
}
