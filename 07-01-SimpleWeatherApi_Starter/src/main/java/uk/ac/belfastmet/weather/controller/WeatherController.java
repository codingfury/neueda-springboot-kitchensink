package uk.ac.belfastmet.weather.controller;

import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import uk.ac.belfastmet.weather.domain.WeatherAPI;
import uk.ac.belfastmet.weather.service.WeatherServiceImpl;

@Controller
@RequestMapping("")
public class WeatherController {

	@Autowired
	WeatherServiceImpl weatherService;
	
	private WeatherAPI todaysWeather;
	
	public WeatherController(WeatherServiceImpl weatherService) {
		super();
		this.weatherService = weatherService;
	}
	
	@GetMapping()
	public String home(Model model) {
		
		model.addAttribute("pageTitle", "Weather");
		RestTemplate restTemplate = new RestTemplate();
		this.todaysWeather = restTemplate.getForObject("http://www.codingfury.net/training/weathersample/weather.php?location=belfast", WeatherAPI.class);
		
		//slf4j logger
		Logger logger = LoggerFactory.getLogger(WeatherController.class); 
		logger.info(this.todaysWeather.toString());
		
		return "weatherForCity";
	}


}
