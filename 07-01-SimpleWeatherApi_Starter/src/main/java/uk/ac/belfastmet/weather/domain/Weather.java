package uk.ac.belfastmet.weather.domain;

import lombok.Data;

@Data
public class Weather {
	
	private String description;
	private String precipitation;
	private Float temperature;
	private String humidity;
	private Integer pressure;
	private Integer cloudcover;
	private Wind wind;
	private String image;
	
}
