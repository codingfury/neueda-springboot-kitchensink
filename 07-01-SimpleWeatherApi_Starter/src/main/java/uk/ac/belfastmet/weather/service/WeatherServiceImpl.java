package uk.ac.belfastmet.weather.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import uk.ac.belfastmet.weather.domain.WeatherAPI;

@Service
public class WeatherServiceImpl implements WeatherService {

	@Value("${api.weatherUrl}") 
	private String weatherUrl;
	
	private RestTemplate restTemplate; 
	
	
	public WeatherServiceImpl(RestTemplate restTemplate) {
		super();
		this.restTemplate = restTemplate;
	}


	public WeatherAPI get(String location) {
		
		UriComponentsBuilder uriBuilder = UriComponentsBuilder
				.fromUriString(weatherUrl)
				.queryParam("location", location);
		
		WeatherAPI todaysWeather = this.restTemplate.getForObject(uriBuilder.toString(), WeatherAPI.class);
		
		
		return todaysWeather;
	}

}
