package uk.ac.belfastmet.weather.domain;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import lombok.Data;

@Data
public class WeatherAPI {
	
	//STEP 1: start with map, and test!
	private Map<String, Object> location;
	private Map<String, Object> weather;

//  STEP 3: replace Map with correct objects	
//	private Location location;
//	private Weather weather;
	
	
	//STEP 2: Use the toString() method to see what's going. 
	public String toString() {
		String locString = this.location.toString();
		String weatherString =this.weather.toString();
		return locString + "\n" + weatherString;
	}
	
}
