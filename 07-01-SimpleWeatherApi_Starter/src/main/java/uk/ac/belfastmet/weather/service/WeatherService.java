package uk.ac.belfastmet.weather.service;

import uk.ac.belfastmet.weather.domain.WeatherAPI;

public interface WeatherService {

	public WeatherAPI get(String location);

	
}
