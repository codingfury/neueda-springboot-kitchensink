package uk.ac.belfastmet.weather.domain;

public class Wind {

	private Float speed;
	private String direction;
	
	public Wind() {
		super();
	}

	public Float getSpeed() {
		return speed;
	}

	public void setSpeed(Float speed) {
		this.speed = speed;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	@Override
	public String toString() {
		return "Wind [speed=" + speed + ", direction=" + direction + "]";
	}

}
