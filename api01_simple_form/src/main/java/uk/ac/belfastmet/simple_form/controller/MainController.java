package uk.ac.belfastmet.simple_form.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

	@GetMapping("")
	public String home(Model model) {
		
		return "index";
	}
	
	@GetMapping("/action")
	public String action(@RequestParam("firstname") String firstName, 
			@RequestParam("lastname") String lastName, Model model) {
		
		model.addAttribute("firstName", firstName);
		model.addAttribute("lastName", lastName);
		
		return "action";
	}
	
}
