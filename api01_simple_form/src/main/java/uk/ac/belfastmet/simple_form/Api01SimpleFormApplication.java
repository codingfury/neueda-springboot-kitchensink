package uk.ac.belfastmet.simple_form;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Api01SimpleFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(Api01SimpleFormApplication.class, args);
	}

}
