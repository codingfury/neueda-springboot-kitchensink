package uk.ac.belfastmet.dwarfs.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import uk.ac.belfastmet.dwarfs.domain.Dwarf;
import uk.ac.belfastmet.dwarfs.repository.DwarfRepository;
import uk.ac.belfastmet.dwarfs.service.DwarfService;

@Controller
@RequestMapping()
public class DwarfController {
	
	DwarfRepository dwarfRepository; 
	
	public DwarfController(DwarfRepository dwarfRepository) {
		super();
		this.dwarfRepository = dwarfRepository;
	}
	
	@GetMapping("/dwarfs")
	public String allDwarfs(Model model) {
		model.addAttribute("pageTitle","All");
		model.addAttribute("dwarfs",this.dwarfRepository.findAll());
		return "dwarfPage";
	}
	
	

	@GetMapping("/view/{dwarfId}")
	public String viewDwarf(@PathVariable("dwarfId") Integer dwarfId, Model model) {
		model.addAttribute("pageTitle","View");
		model.addAttribute("dwarf",this.dwarfRepository.findByDwarfId(dwarfId));
		return "viewDwarf";
	}
	
	@GetMapping("/delete/{dwarfId}")
	public String deleteDwarf(@PathVariable("dwarfId") Integer dwarfId, Model model
			, RedirectAttributes redirectAttributes) {
		
		redirectAttributes.addFlashAttribute("message", "Successfully deleted dwarf!");
		this.dwarfRepository.delete(dwarfId);
		return "redirect:/dwarfs";
	}
	
	
	
}
