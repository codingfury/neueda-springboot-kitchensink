package com.codingfury.swaggerclient;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerClientApplication {

	private static final Logger logger = LoggerFactory.getLogger(SwaggerClientApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(SwaggerClientApplication.class, args);
	}
	
	@PostConstruct
	void printSomeStuff() {
		logger.info("Hello World");
	}
}
