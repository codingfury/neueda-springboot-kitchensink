package com.codingfury.briantitanic.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;


@Entity
@Data
public class Passenger {

	@Id
	Long passengerId;
	Long survived;
	Long pclass;	
	String name;	
	String sex;	
	Long age;	
	Long sibsp;	
	Long parch;	
	String ticket;	
	Float fare;
	String cabin;	
	String embarked;
	
}
