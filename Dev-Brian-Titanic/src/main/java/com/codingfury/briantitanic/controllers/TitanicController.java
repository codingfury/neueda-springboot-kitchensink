package com.codingfury.briantitanic.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import com.codingfury.briantitanic.repositories.PassengerRepository;


@Controller
@RequestMapping("/")
public class TitanicController {
	
	@Autowired
	PassengerRepository  passengerRepository;
	
	@GetMapping("/")
    public String home(Model model) {
		model.addAttribute("pageTitle", "Titanic Passengers");
		model.addAttribute("passengers", passengerRepository.findAll());
		return "titanicPage";
	}

	
	
}

