package com.codingfury.briantitanic.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.briantitanic.domain.Passenger;

public interface PassengerRepository extends CrudRepository<Passenger, Long> {

}
