package com.codingfury.cereal.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;


@Entity
public class Cereal {

	@Id
	@GeneratedValue
	private Integer cerealId;
	@NotEmpty
	private String manufacturer;
	@NotEmpty
	private String name;
	private Integer energy;
	private Integer calories;
	private Double protein;
	private Double carbohydrate;
	private Double sugars;
	private Double fat;
	private Double saturates;
	private Double fibre;
	private String sodium;
	private String salt;
	private Double iron;
	

	public Cereal() {
		super();
	}


	public Integer getCerealId() {
		return cerealId;
	}


	public void setCerealId(Integer cerealId) {
		this.cerealId = cerealId;
	}


	public String getManufacturer() {
		return manufacturer;
	}


	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getEnergy() {
		return energy;
	}


	public void setEnergy(Integer energy) {
		this.energy = energy;
	}


	public Integer getCalories() {
		return calories;
	}


	public void setCalories(Integer calories) {
		this.calories = calories;
	}


	public Double getProtein() {
		return protein;
	}


	public void setProtein(Double protein) {
		this.protein = protein;
	}


	public Double getCarbohydrate() {
		return carbohydrate;
	}


	public void setCarbohydrate(Double carbohydrate) {
		this.carbohydrate = carbohydrate;
	}


	public Double getSugars() {
		return sugars;
	}


	public void setSugars(Double sugars) {
		this.sugars = sugars;
	}


	public Double getFat() {
		return fat;
	}


	public void setFat(Double fat) {
		this.fat = fat;
	}


	public Double getSaturates() {
		return saturates;
	}


	public void setSaturates(Double saturates) {
		this.saturates = saturates;
	}


	public Double getFibre() {
		return fibre;
	}


	public void setFibre(Double fibre) {
		this.fibre = fibre;
	}


	public String getSodium() {
		return sodium;
	}


	public void setSodium(String sodium) {
		this.sodium = sodium;
	}


	public String getSalt() {
		return salt;
	}


	public void setSalt(String salt) {
		this.salt = salt;
	}


	public Double getIron() {
		return iron;
	}


	public void setIron(Double iron) {
		this.iron = iron;
	}


	@Override
	public String toString() {
		return "Cereal [cerealId=" + cerealId + ", manufacturer=" + manufacturer + ", name=" + name + ", energy="
				+ energy + ", calories=" + calories + ", protein=" + protein + ", carbohydrate=" + carbohydrate
				+ ", sugars=" + sugars + ", fat=" + fat + ", saturates=" + saturates + ", fibre=" + fibre + ", sodium="
				+ sodium + ", salt=" + salt + ", iron=" + iron + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((calories == null) ? 0 : calories.hashCode());
		result = prime * result + ((carbohydrate == null) ? 0 : carbohydrate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((cerealId == null) ? 0 : cerealId.hashCode());
		result = prime * result + ((energy == null) ? 0 : energy.hashCode());
		result = prime * result + ((fat == null) ? 0 : fat.hashCode());
		result = prime * result + ((fibre == null) ? 0 : fibre.hashCode());
		result = prime * result + ((iron == null) ? 0 : iron.hashCode());
		result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((protein == null) ? 0 : protein.hashCode());
		result = prime * result + ((salt == null) ? 0 : salt.hashCode());
		result = prime * result + ((saturates == null) ? 0 : saturates.hashCode());
		result = prime * result + ((sodium == null) ? 0 : sodium.hashCode());
		result = prime * result + ((sugars == null) ? 0 : sugars.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cereal other = (Cereal) obj;
		if (calories == null) {
			if (other.calories != null)
				return false;
		} else if (!calories.equals(other.calories))
			return false;
		if (carbohydrate == null) {
			if (other.carbohydrate != null)
				return false;
		} else if (!carbohydrate.equals(other.carbohydrate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (cerealId == null) {
			if (other.cerealId != null)
				return false;
		} else if (!cerealId.equals(other.cerealId))
			return false;
		if (energy == null) {
			if (other.energy != null)
				return false;
		} else if (!energy.equals(other.energy))
			return false;
		if (fat == null) {
			if (other.fat != null)
				return false;
		} else if (!fat.equals(other.fat))
			return false;
		if (fibre == null) {
			if (other.fibre != null)
				return false;
		} else if (!fibre.equals(other.fibre))
			return false;
		if (iron == null) {
			if (other.iron != null)
				return false;
		} else if (!iron.equals(other.iron))
			return false;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (protein == null) {
			if (other.protein != null)
				return false;
		} else if (!protein.equals(other.protein))
			return false;
		if (salt == null) {
			if (other.salt != null)
				return false;
		} else if (!salt.equals(other.salt))
			return false;
		if (saturates == null) {
			if (other.saturates != null)
				return false;
		} else if (!saturates.equals(other.saturates))
			return false;
		if (sodium == null) {
			if (other.sodium != null)
				return false;
		} else if (!sodium.equals(other.sodium))
			return false;
		if (sugars == null) {
			if (other.sugars != null)
				return false;
		} else if (!sugars.equals(other.sugars))
			return false;
		return true;
	}
	
	
}


