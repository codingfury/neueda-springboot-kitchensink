package com.codingfury.cereal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.codingfury.cereal.domain.Cereal;
import com.codingfury.cereal.repositories.CerealRepository;

@Controller
@RequestMapping("/")
public class MainController {
	
	@Autowired 
	CerealRepository cerealRepository; 
	
	
	public MainController(CerealRepository cerealRepository) {
		super();
		this.cerealRepository = cerealRepository;
	}


	@GetMapping("/")
	public String home(Model model) {
		
		model.addAttribute("pageTitle", "Home");
		model.addAttribute("cereals", cerealRepository.findAll());
		
		return "homePage";
	}
	
	@GetMapping("/cereal/view/{cerealId}")
	public String viewCereal(@PathVariable Integer cerealId, Model model) {
		
		model.addAttribute("pageTitle", "View Cereal");
		model.addAttribute("cereal", cerealRepository.findOne(cerealId));
		return "viewCereal";
	}

	@GetMapping("/cereal/add")
	public String createCereal(Model model) {
		
		model.addAttribute("pageTitle", "Create Cereal");
		model.addAttribute("title", "Create");

		model.addAttribute("cereal", new Cereal()); 
		
		return "editCereal";
	}
	//NO VALIDATION
//	@PostMapping("/cereal/save")
//	public String save(Cereal cereal) {
//		
//		Cereal savedCereal = cerealRepository.save(cereal);
//		
//		return "redirect:/cereal/view/" + savedCereal.getCerealId();
//	}

	
	//WITH VALIDATION
	@PostMapping("/cereal/save")
	public String save(@Valid Cereal cereal, BindingResult bindingResult, Model model) {
		
		if (bindingResult.hasErrors())
		{
			return "editCereal";
			
		}else {
			Cereal savedCereal = cerealRepository.save(cereal);
			return "redirect:/cereal/view/" + savedCereal.getCerealId(); 
			// redirect means that the GetMapping function for the cereal/view (above) gets called, 
			// rather than just loading the html template
		}

	}
	
	@GetMapping("/cereal/edit/{cerealId}")
	public String editCereal(@PathVariable Integer cerealId, Model model) {
		
		model.addAttribute("pageTitle", "Edit Cereal");
		model.addAttribute("cereal", cerealRepository.findOne(cerealId));
		return "editCereal"; //note hidden cerealId field now comes into play. 
	}
	
	@GetMapping("/cereal/delete/{cerealId}")
	public String deleteCereal(@PathVariable Integer cerealId, RedirectAttributes redirectAttrs) {
		
		cerealRepository.delete(cerealId);
		redirectAttrs.addFlashAttribute("message", "Cereal was deleted!");

		return "redirect:/"; 
	}
	

}
