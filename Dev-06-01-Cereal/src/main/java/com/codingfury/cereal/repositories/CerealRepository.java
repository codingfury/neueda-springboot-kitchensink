package com.codingfury.cereal.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.cereal.domain.Cereal;

public interface CerealRepository extends CrudRepository<Cereal, Integer> {

	Cereal findByCerealId(int i);

}
