package com.codingfury.springweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuruCh02SpringWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(GuruCh02SpringWebApplication.class, args);
	}
}
