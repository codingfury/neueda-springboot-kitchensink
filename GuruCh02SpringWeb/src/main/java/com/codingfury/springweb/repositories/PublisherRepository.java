package com.codingfury.springweb.repositories;

import org.springframework.data.repository.CrudRepository;

import com.codingfury.springweb.model.Publisher;

public interface PublisherRepository extends CrudRepository<Publisher, Long> {

}
